import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';

class AvatarImagePicker extends StatefulWidget {
  final Function onUpdateImage;
  File? image;
  final String? url;

   AvatarImagePicker(
      {Key? key, this.image, required this.onUpdateImage, this.url})
      : super(key: key);

  @override
  State<AvatarImagePicker> createState() => _AvatarImagePickerState();
}

class _AvatarImagePickerState extends State<AvatarImagePicker> {
  final ImagePicker _picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomRight,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(8.sp),
          child: SizedBox(
            height: 150.r,
            width: 150.r,
            child: InkWell(
              onTap: () {
                FocusScope.of(context).unfocus();
                _showBottomSheetImagePicker(context);
              },
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: getImageWidget(),
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 15.sp,
          child: Container(
            height: 40.sp,
            width: 40.sp,
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            padding: EdgeInsets.all(3.sp),
            child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: Colors.grey[200]),
              child: IconButton(
                icon: Icon(
                  Icons.add,
                  color: Theme.of(context).colorScheme.secondary,
                  size: 19.sp,
                ),
                onPressed: () {
                  FocusScope.of(context).unfocus();
                  _showBottomSheetImagePicker(context);
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget getImageWidget() {
    if (widget.image == null && widget.url == null) {
      return Icon(Icons.person, size: 90.sp);
    } else if (widget.image != null) {
      return Image.file(
        widget.image!,
        fit: BoxFit.cover,
      );
    }
    try {
      return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          border: Border.all(color: Colors.black),
        ),
        padding: EdgeInsets.all(1.sp),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10000.0),
          child: CachedNetworkImage(
            fit: BoxFit.cover,
            width: 75.r,
            height: 75.r,
            imageUrl: widget.url ?? "",
            errorWidget: (context, url, error) => Padding(
              padding: const EdgeInsets.all(1.0),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: const Icon(Icons.person)),
            ),
          ),
        ),
      );
    } catch (e) {
      return const Icon(Icons.cancel_rounded);
    }
  }

  void _showBottomSheetImagePicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Wrap(
              children: <Widget>[
                ListTile(
                    leading: const Icon(
                      Icons.photo_library,
                      // color: myColors.orange,
                    ),
                    title: const Text("gallery"),
                    onTap: () async {
                      _imagePicker(ImageSource.gallery);
                      Navigator.of(context).pop();
                    }),
                ListTile(
                  leading: const Icon(
                    Icons.photo_camera,
                    // color: myColors.orange,
                  ),
                  title: const Text("camera"),
                  onTap: () async {
                    _imagePicker(ImageSource.camera);
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        });
  }

  Future<void> _imagePicker(ImageSource imageSource) async {
    XFile? temp =
        await _picker.pickImage(source: imageSource, imageQuality: 10);
    if (temp != null) {
      widget.onUpdateImage(mobileImage: File(temp.path));
      setState(() {
        widget.image = File(temp.path);
      });
    }
  }
}
