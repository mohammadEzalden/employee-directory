import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {
  final double size;
  final String? url;

  const Avatar({Key? key, this.url, this.size = 30}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (url == null) {
      return Container(
        height: size,
        width: size,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.grey),
        ),
        child: Padding(
          padding: const EdgeInsets.all(1.0),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: const Icon(Icons.person)),
        ),
      );
    }
    try {
      return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          border: Border.all(color: Colors.black),
        ),
        padding: const EdgeInsets.all(1),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10000.0),
          child: CachedNetworkImage(
            fit: BoxFit.cover,
            width: size,
            height: size,
            imageUrl: url!,
            errorWidget: (context, url, error) => Padding(
              padding: const EdgeInsets.all(1.0),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: const Icon(Icons.person)),
            ),
          ),
        ),
      );
    } catch (e) {
      return const Icon(Icons.cancel_rounded);
    }
  }
}
