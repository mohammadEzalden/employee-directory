class Employee {
  final String id;
  final String name;
  final String jobTitle;
  final String email;
  final String phoneNumber;
  String? photoURL;

  Employee({
    required this.id,
    required this.name,
    required this.jobTitle,
    required this.email,
    required this.phoneNumber,
    this.photoURL,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'job_title': jobTitle,
      'email': email,
      'phone_number': phoneNumber,
      'photoURL': photoURL,
    };
  }

  factory Employee.fromJson(Map<String, dynamic> json, String id) {
    return Employee(
      id: id,
      name: json['name'],
      jobTitle: json['job_title'],
      email: json['email'],
      phoneNumber: json['phone_number'],
      photoURL: json['photoURL'],
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Employee && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;
}
