import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:employee_directory/data/empolyee/model/employee.dart';

class EmployeeApi {
  FirebaseFirestore fireStore;

  EmployeeApi({required this.fireStore});

  Future<Employee> createEmployee({required Map<String, dynamic> body}) async {
    CollectionReference employeesCollection =
        FirebaseFirestore.instance.collection('employees');
    DocumentReference documentReference = await employeesCollection.add(body);
    return Employee.fromJson(body, documentReference.id);
  }

  Future<List<Employee>> getEmployees() async {
    CollectionReference employeesCollection = fireStore.collection('employees');
    QuerySnapshot querySnapshot = await employeesCollection.get();
    List<Employee> employees = querySnapshot.docs.map((doc) {
      Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
      String id = doc.id;
      return Employee.fromJson(data, id);
    }).toList();
    return employees;
  }

  Future<bool> editEmployee(
      {required Map<String, dynamic> body, required String employeeId}) async {
    CollectionReference employeesCollection =
        FirebaseFirestore.instance.collection('employees');
    bool res = false;
    await employeesCollection.doc(employeeId).update(body).then((value) {
      res = true;
    }).catchError((error) => print("Failed to update employee: $error"));
    return res;
  }

  deleteEmployee({required String id}) async {
    FirebaseFirestore.instance.collection('employees').doc(id).delete();
  }

  Future<List<Employee>> search({required String search}) async {
    List<Employee> res = [];
    res.addAll(await fireStore
        .collection('employees')
        .orderBy('name')
        .startAt([search])
        .endAt(['$search\uf8ff'])
        .get()
        .then((querySnapshot) {
          List<Employee> employees = querySnapshot.docs.map((doc) {
            Map<String, dynamic> data = doc.data();
            String id = doc.id;
            return Employee.fromJson(data, id);
          }).toList();
          return employees;
        }));

    res.addAll(await fireStore
        .collection('employees')
        .orderBy('job_title')
        .startAt([search])
        .endAt(['$search\uf8ff'])
        .get()
        .then((querySnapshot) {
          List<Employee> employees = querySnapshot.docs.map((doc) {
            Map<String, dynamic> data = doc.data();
            String id = doc.id;
            return Employee.fromJson(data, id);
          }).toList();
          return employees;
        }));
    res.addAll(await fireStore
        .collection('employees')
        .orderBy('email')
        .startAt([search])
        .endAt(['$search\uf8ff'])
        .get()
        .then((querySnapshot) {
          List<Employee> employees = querySnapshot.docs.map((doc) {
            Map<String, dynamic> data = doc.data();
            String id = doc.id;
            return Employee.fromJson(data, id);
          }).toList();
          return employees;
        }));
    return res.toSet().toList();
  }
}
