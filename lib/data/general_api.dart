import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';

class GeneralApi {
  FirebaseStorage storage;

  GeneralApi({required this.storage});

  Future<String> uploadAvatar({required File file}) async {
    final storageRef = FirebaseStorage.instance.ref();
    final employeeAvatar = storageRef
        .child("employee")
        .child("avatars")
        .child(file.path.split("/").last);
    try {
      await employeeAvatar.putFile(file);
      return await employeeAvatar.getDownloadURL();
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}
