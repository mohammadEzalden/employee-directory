import 'package:employee_directory/screens/employee/widgets/employee_item.dart';
import 'package:employee_directory/view_model/employee_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Employees"),
      ),
      body: RefreshIndicator(
        onRefresh: Provider.of<EmployeeViewModel>(context, listen: false)
            .refreshEmployees,
        child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: Column(
            children: [
              10.verticalSpace,
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.w),
                child: Container(
                  width: 1.sw,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            blurRadius: 10,
                            offset: const Offset(0, 0),
                            color: Colors.black.withOpacity(0.05),
                            spreadRadius: 0)
                      ],
                      borderRadius: BorderRadius.circular(8.sp)),
                  child: TextFormField(
                    onTap: () {
                      Navigator.pushNamed(context, "/search_screen");
                    },
                    readOnly: true,
                    decoration: InputDecoration(

                        contentPadding: EdgeInsets.symmetric(horizontal: 8.sp,vertical: 15.sp),
                        prefixIcon: const Icon(
                          Icons.search,
                          color: Colors.grey,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[200]!),
                          borderRadius: BorderRadius.circular(5.sp),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[200]!),
                          borderRadius: BorderRadius.circular(5.sp),
                        ),
                        hintText: 'Search employees'),
                  ),
                ),
              ),
              Consumer<EmployeeViewModel>(
                builder: (context, employeeModel, child) {
                  if (employeeModel.loadingEmployees) {
                    return const CircularProgressIndicator();
                  }
                  if (employeeModel.failure != null) {
                    return Text(employeeModel.mapFailureToMessage());
                  }
                  return ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: employeeModel.allEmployees.length,
                    itemBuilder: (context, index) {
                      return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: EmployeeItem(
                            employee: employeeModel.allEmployees[index],
                          ));
                    },
                  );
                },
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, "/add_employee_screen");
          },
          child: const Icon(Icons.add)),
    );
  }
}
