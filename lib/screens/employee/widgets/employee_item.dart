import 'package:employee_directory/data/empolyee/model/employee.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../core/utili/widgets/avatar.dart';

class EmployeeItem extends StatefulWidget {
  final Employee employee;

  const EmployeeItem({Key? key, required this.employee}) : super(key: key);

  @override
  State<EmployeeItem> createState() => _EmployessItemState();
}

class _EmployessItemState extends State<EmployeeItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, "/employee_details",
            arguments: widget.employee);
      },
      child: Card(
        child: Padding(
          padding: EdgeInsets.all(8.sp),
          child: Row(
            children: [
              Avatar(url: widget.employee.photoURL, size: 65.r),
              30.horizontalSpaceRadius,
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      widget.employee.name,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 22.sp),
                    ),
                    Text(
                      widget.employee.jobTitle,
                      style: TextStyle(color: Colors.grey, fontSize: 14.sp),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          widget.employee.email,
                          style: TextStyle(color: Colors.grey, fontSize: 14.sp),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          widget.employee.phoneNumber,
                          style: TextStyle(color: Colors.grey, fontSize: 14.sp),
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
