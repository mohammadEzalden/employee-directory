import 'package:employee_directory/screens/employee/search/EmptySearchWidget.dart';
import 'package:employee_directory/view_model/employee_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../widgets/employee_item.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  final GlobalKey<FormState> fromKey = GlobalKey<FormState>();

  final TextEditingController query = TextEditingController();

  FocusNode focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        actions: [
          IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(Icons.arrow_forward))
        ],
        leading: 0.verticalSpace,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: Form(
              key: fromKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  10.verticalSpace,
                  Text(
                    "Search",
                    style:
                        TextStyle(fontSize: 35.sp, fontWeight: FontWeight.bold),
                  ),
                  30.verticalSpace,
                  TextFormField(
                    controller: query,
                    onChanged: (value) async {
                      if (value.isEmpty) return;
                      search();
                    },
                    style:  TextStyle(fontSize: 14.sp),
                    focusNode: focusNode,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 8.sp,vertical: 15.sp),
                        prefixIcon: const Icon(Icons.search),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue[200]!),
                          borderRadius: BorderRadius.circular(5.sp),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[200]!),
                          borderRadius: BorderRadius.circular(5.sp),
                        ),
                        hintStyle:  TextStyle(color: Colors.black,fontSize: 14.sp),
                        hintText: 'Search services and clinics'),
                  ),
                  30.verticalSpace,
                  Consumer<EmployeeViewModel>(
                    builder: (context, searchModel, child) {
                      if (searchModel.searching) {
                        return const CircularProgressIndicator();
                      }
                      return searchModel.searchResult.isEmpty
                          ? const EmptySearchWidget()
                          : ListView(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              children: <Widget>[
                                ListTile(
                                  dense: true,
                                  contentPadding:
                                      const EdgeInsets.symmetric(vertical: 0),
                                  title: Text(
                                    "Showing ${searchModel.searchResult.length} results",
                                    style: TextStyle(fontSize: 17.sp),
                                  ),
                                ),
                                ListView.builder(
                                  shrinkWrap: true,
                                  primary: false,
                                  itemCount: searchModel.searchResult.length,
                                  itemBuilder: (context, index) {
                                    if (searchModel.searchResult.isEmpty) {
                                      return const SizedBox.shrink();
                                    }
                                    return Padding(
                                        padding: EdgeInsets.only(bottom: 20.sp),
                                        child: EmployeeItem(
                                          employee:
                                              searchModel.searchResult[index],
                                        ));
                                  },
                                ),
                              ],
                            );
                    },
                  ),
                  39.verticalSpace,
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  search() async {
    if (fromKey.currentState?.validate() ?? false) {
      await Provider.of<EmployeeViewModel>(context, listen: false)
          .search(search: query.text);
    }
  }
}
