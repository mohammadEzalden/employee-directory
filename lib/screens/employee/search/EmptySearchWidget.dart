import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class EmptySearchWidget extends StatefulWidget {
  const EmptySearchWidget({
    Key? key,
  }) : super(key: key);

  @override
  _EmptySearchWidgetState createState() => _EmptySearchWidgetState();
}

class _EmptySearchWidgetState extends State<EmptySearchWidget> {
  bool loading = true;

  @override
  void initState() {
    Timer(const Duration(seconds: 3), () {
      if (mounted) {
        setState(() {
          loading = false;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        loading
            ? SizedBox(
                height: 3,
                child: LinearProgressIndicator(
                  backgroundColor: Colors.black.withOpacity(0.2),
                  // color:Theme.of(context),
                ),
              )
            : const SizedBox(),
        Container(
          alignment: AlignmentDirectional.center,
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              60.verticalSpace,
               Text("Not Found", style: TextStyle(fontSize: 20.sp),),
               Text(
                "We're sorry, your search could not be found.\nPlease try with another keyword.",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14.sp),
              ),
              40.verticalSpace,
            ],
          ),
        )
      ],
    );
  }
}
