import 'dart:io';
import 'package:employee_directory/view_model/employee_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import '../../core/utili/constants/helper.dart';
import '../../core/utili/widgets/avatar_image_picker.dart';

class AddEmployeeScreen extends StatefulWidget {
  const AddEmployeeScreen({Key? key}) : super(key: key);

  @override
  State<AddEmployeeScreen> createState() => _AddEmployeeScreenState();
}

class _AddEmployeeScreenState extends State<AddEmployeeScreen> {
  final TextEditingController name = TextEditingController();
  final TextEditingController jobTitle = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController phone = TextEditingController();
  File? image;

  final GlobalKey<FormState> fromKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add New Employee"),
      ),
      body: Padding(
        padding: EdgeInsets.all(20.sp),
        child: SingleChildScrollView(
          child: Form(
            key: fromKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AvatarImagePicker(
                      onUpdateImage: ({File? mobileImage}) async {
                        image = mobileImage;
                      },
                      image: image,
                    ),
                  ],
                ),
                Text(
                  "Name",
                  style: TextStyle(fontSize: 14.sp),
                ),
                5.verticalSpace,
                TextFormField(
                  controller: name,
                  validator: (input) => input!.length < 3
                      ? "should be more than 3 letters"
                      : null,
                  style: TextStyle(fontSize: 14.sp),
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 8.sp, vertical: 15.sp),
                    hintText: "Enter name ",
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
                20.verticalSpace,
                Text(
                  "Job Title",
                  style: TextStyle(fontSize: 14.sp),
                ),
                5.verticalSpace,
                TextFormField(
                  controller: jobTitle,
                  validator: (input) => input!.length < 3
                      ? "should be more than 3 letters"
                      : null,
                  style: TextStyle(fontSize: 14.sp),
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 8.sp, vertical: 15.sp),
                    hintText: "Enter Job Title",
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
                20.verticalSpace,
                Text(
                  "Phone number",
                  style: TextStyle(fontSize: 14.sp),
                ),
                5.verticalSpace,
                TextFormField(
                  controller: phone,
                  style: TextStyle(fontSize: 14.sp),
                  validator: (input) =>
                      input!.length < 9 ? "should be more than 9 digits" : null,
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 8.sp, vertical: 15.sp),
                    hintText: "Phone",
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
                20.verticalSpace,
                Text(
                  "Email",
                  style: TextStyle(fontSize: 14.sp),
                ),
                5.verticalSpace,
                TextFormField(
                  controller: email,
                  validator: (t) {
                    // if (t!=null&&t.isNotEmpty&&!Helper.isEmail(t.trim())) {
                    if (t != null && !Helper.isEmail(t.trim())) {
                      return "should be a valid email";
                    }
                    return null;
                  },
                  style: TextStyle(fontSize: 14.sp),
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 8.sp, vertical: 15.sp),
                    hintText: "Email",
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
                20.verticalSpace,
                Center(
                    child: context.watch<EmployeeViewModel>().addingEmployee
                        ? const CircularProgressIndicator()
                        : SizedBox(
                            width: 0.25.sw,
                            height: 50.r,
                            child: ElevatedButton(
                                onPressed: () async {
                                  if (fromKey.currentState!.validate()) {
                                    await context
                                        .read<EmployeeViewModel>()
                                        .addEmployee(body: {
                                      "name": name.text,
                                      "job_title": jobTitle.text,
                                      "email": email.text,
                                      "phone_number": phone.text,
                                    }, image: image);
                                    if (mounted) {
                                      Navigator.pop(context);
                                    }
                                  }
                                },
                                child: Text(
                                  "Add Employee",
                                  style: TextStyle(fontSize: 20.sp),
                                )),
                          ))
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    name.dispose();
    jobTitle.dispose();
    email.dispose();
    phone.dispose();
    super.dispose();
  }
}
