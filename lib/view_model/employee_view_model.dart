import 'dart:io';
import 'package:employee_directory/core/exceptions/failures.dart';
import 'package:employee_directory/data/empolyee/api/employee_api.dart';
import 'package:employee_directory/data/empolyee/model/employee.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../data/general_api.dart';

class EmployeeViewModel extends ChangeNotifier {
  EmployeeApi employeeApi;
  GeneralApi generalApi;

  late List<Employee> allEmployees;
  late List<Employee> searchResult;

  ///Error handler
  Failure? failure;

  ///variables to control the visibility of the spinner when necessary.
  late bool loadingEmployees;
  late bool addingEmployee;
  late bool editingEmployee;
  late bool searching;

  EmployeeViewModel({required this.employeeApi, required this.generalApi}) {
    failure = null;
    editingEmployee = false;
    addingEmployee = false;
    searching = false;
    loadingEmployees = true;
    allEmployees = [];
    searchResult = [];
    getAllEmployees();
  }

  getAllEmployees() async {
    try {
      allEmployees = await employeeApi.getEmployees();
    } catch (e) {
      failure = ServerFailure();
    }
    loadingEmployees = false;
    notifyListeners();
  }

  Future<void> addEmployee(
      {required Map<String, dynamic> body, File? image}) async {
    addingEmployee = true;
    notifyListeners();
    try {
      if (image != null) {
        body["photoURL"] = await uploadImage(image);
      }
      allEmployees.add(await employeeApi.createEmployee(body: body));
    } on ServerFailure {
      failure = ServerFailure();
    } catch (e) {
      // Handle other exceptions
    }
    addingEmployee = false;
    notifyListeners();
  }

  Future<void> editEmployee(
      {required Map<String, dynamic> body,
      File? image,
      required String id}) async {
    editingEmployee = true;
    notifyListeners();
    try {
      if (image != null) {
        body["photoURL"] = await uploadImage(image);
      }
      int index = allEmployees.indexWhere((element) => element.id == id);
      allEmployees.removeAt(index);
      if (await employeeApi.editEmployee(body: body, employeeId: id)) {
        allEmployees.add(Employee.fromJson(body, id));
      }
    }on ServerFailure {
      failure = ServerFailure();
    } catch (e) {
      // Handle other exceptions
    }
    editingEmployee = false;
    notifyListeners();
  }

  Future<void> deleteEmployee({required String id}) async {
    allEmployees.removeWhere((element) => element.id == id);
    await employeeApi.deleteEmployee(id: id);
    notifyListeners();
  }

  Future<String> uploadImage(File image) async {
    try {
      return await generalApi.uploadAvatar(file: image);
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<void> refreshEmployees() async {
    loadingEmployees = true;
    notifyListeners();
    await getAllEmployees();
  }

  search({String? search}) async {
    if (search == null) return;
    searching = true;
    searchResult.clear();
    notifyListeners();
    searchResult = await employeeApi.search(search: search);
    searching = false;
    notifyListeners();
  }

  String mapFailureToMessage() {
    switch (failure.runtimeType) {
      case ServerFailure:
        return "SERVER_FAILURE_MESSAGE";
      case OfflineFailure:
        return "OFFLINE_FAILURE_MESSAGE";
      default:
        return "Unexpected Error , Please try again later .";
    }
  }
}
