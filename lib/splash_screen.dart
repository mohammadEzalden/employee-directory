import 'dart:async';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  final String? invoiceId;

  const SplashScreen({Key? key, this.invoiceId}) : super(key: key);

  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  startTime() async {
    var duration = const Duration(milliseconds: 10);
    return Timer(duration, navigationPage);
  }

  void navigationPage() async {
    Navigator.pushReplacementNamed(context, '/HomeScreen');
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[50],
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Text("Employee Directory LOGO"),
          ],
        ),
      ),
    );
  }
}
