import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:employee_directory/data/empolyee/api/employee_api.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get_it/get_it.dart';
import 'data/general_api.dart';
import 'view_model/employee_view_model.dart';

final si = GetIt.instance;

Future<void> init() async {
  /// Core

  final storage = FirebaseStorage.instance;
  si.registerLazySingleton(() => storage);

  FirebaseFirestore fireStore = FirebaseFirestore.instance;
  si.registerLazySingleton(() => fireStore);

  /// Provider

  si.registerLazySingleton(
      () => EmployeeViewModel(employeeApi: si(), generalApi: si()));

  /// APIs Call
  si.registerLazySingleton(() => EmployeeApi(fireStore: si()));
  si.registerLazySingleton(() => GeneralApi(storage: si()));
}
