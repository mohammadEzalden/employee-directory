import 'package:employee_directory/data/empolyee/model/employee.dart';
import 'package:employee_directory/screens/employee/add_employee_screen.dart';
import 'package:employee_directory/screens/employee/employee_details.dart';
import 'package:employee_directory/screens/employee/search/search_screen.dart';
import 'package:employee_directory/screens/home_screen.dart';
import 'package:employee_directory/splash_screen.dart';
import 'package:employee_directory/view_model/employee_view_model.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:employee_directory/injectionContaner.dart' as di;
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {

  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
              create: (context) => di.si<EmployeeViewModel>()),
        ],
        child: ScreenUtilInit(
          designSize: const Size(428, 925),
          minTextAdapt: true,
          splitScreenMode: true,
          builder: (context, child) {
            return MaterialApp(
              debugShowCheckedModeBanner: false,
              home: child,
              onGenerateRoute: (settings) {
                var view;
                switch (settings.name) {
                  case '/HomeScreen':
                    view = const HomeScreen();
                    break;
                  case '/add_employee_screen':
                    view = const AddEmployeeScreen();
                    break;
                  case '/search_screen':
                    view = const SearchScreen();
                    break;
                  case '/employee_details':
                    view = EmployeeDetails(employee: settings.arguments as Employee);
                    break;
                }
                return MaterialPageRoute(
                    builder: (context) => view, settings: settings);
              },
            );
          },
          child: const SplashScreen(),
        ));
  }
}
